# EDSX-API-PRODUITS

-----------------------

### Liste les produits par nom

Methode 1:

    
    var API = require('edsx-api-produits');

    API.getByName(search, options, function(err, resp){
        // Do something
    })
    

Methode 2:

    var API = require('edsx-api-produits');

    API.getByName(search, options).then(function(resp) {
        // Do something
    }, function(err) {
       console.log('err')
    }) 

### Exemples

    var API = require('edsx-api-produits');

    // Pour la methode 1:
    API.getByName('raccords', {type: 3, limit: 10}, function(err, res){
        console.log(res);
    })

    // Pour la methode 2:
    API.getByName('inter', {type: 1, limit: 10})
        .then(function(resp) {
            console.log(resp)
        }, function(err) {
            console.log(err)
    })

### Return

    {
        nom: String,
        image: String,
        prix: Number,
        objets: [{
            ref: Number,
            nom: String,
            prix: Number,
            dispo: String
        }]
    }

/!\ Il peut y avoir plusieurs [object] dans objets /!\ 

### Options disponibles

    {
        type: 0,            // 1/2/3/EL/PL/SE - Default: None
        limit: 10           // 1 - 100 - Default: 10 
    }
