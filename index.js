var request = require('request')
var Q = require('q')

module.exports = {
    getByName: function (search, options, callback) {
        var deferred = Q.defer();

        var url = 'http://23.97.228.61:8080/api/autocomplete?term='+search;

        if (options.type != undefined) 
        	url = url+'&type='+options.type;
        if (options.limit != undefined)
        	url = url+'&limit='+options.limit;

		request(url, function(err, res){
			if (!err)
				deferred.resolve(res.body);
			else
				deferred.reject(err);
		})

        deferred.promise.nodeify(callback);
        return deferred.promise;
    }
}